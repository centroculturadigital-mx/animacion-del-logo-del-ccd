# Animación del logo del CCD

## Prueba

### Animación del Logo del CCD

#### Instrucciones para participar

- Solicita acceso en el link debajo del nombre "Request Access"
- Clona el repo esta carpeta contiene (un svg de logo, un archivo html, un archivo de css y un markdown para que documentes tu proceso)
- Crea una rama con tu nombre
- Trabaja tu código
- Cuando esté lista tu prueba, haz push y crea una merge request
- Envía un correo a raquelglucio.ccd@gmail.com

#### Qué evaluaremos

- Creatividad
- Solución de problemas
- Código ordenado y fácil de leer
- Documentación legible
- *Considera que este código debe estar pensado para poder ser integrado a un proyecto de React.js*

## Sobre el logo

El logo se construye con ocho nodos dispuestos en forma de círculo que se unen entre sí por líneas rectas

### Reglas:

- Los nodo no pueden cambiar de posición
- Las líneas de unión sólo pueden ser líneas rectas
- Las líneas pueden nacer del centro del logo o de los nodos
- Cada nodo puede originar o conectar líneas únicamente desde tres puntos (posición inferior, derecha o izquierda)
- Todos los círculos deben estar unidos entre sí
- Ningún círculo puede quedar sin unir

## Requerimientos:

- Animación svg, css y javascript
- Redimensionable
- Responsivo
- Conexión de todos los puntos en hover

## Ejemplo

El código del logo animado actual de la página está creado con css, acá el cógigo
<div>
<p class="codepen" data-height="300" data-default-tab="result" data-slug-hash="yVOLvY" data-user="Glucio" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span><a href="https://codepen.io/Glucio/pen/yVOLvY">
  Logo CCD</a> </span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
</div>
